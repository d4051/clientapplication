package com.courseregistration.app;

public class Constants {


    public static final String SERVER_IP = "100.64.00.00";
    public static final int SERVER_PORT = 10001;
    public static final String SERVICE_NAME = "Course_Registration";

    public static final String ID = "id";
    public static final String USER_TYPE = "user_type";
    public static final String ACTION = "action";
    public static final String REQUEST_TYPE_WRITE = "write";
    public static final String REQUEST_TYPE_READ = "read";
    public static final String REGISTER_COURSE = "register_course";
    public static final String DEREGISTER_COURSE = "deregister_course";
    public static final String GET_MY_COURSES = "get_my_course";
    public static final String GET_AVAILABLE_COURSES = "get_available_courses";
    public static final String ADD_COURSE = "add_course";
    public static final String VIEW_COURSE_DETAILS = "view_course_details";
    public static final String USER_STUDENT = "user_student";
    public static final String USER_PROFESSOR = "user_professor";
}
