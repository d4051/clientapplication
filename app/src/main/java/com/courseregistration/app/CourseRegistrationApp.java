package com.courseregistration.app;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.courseregistration.app.nsd.NsdHelper;
import com.courseregistration.app.sharedpreference.SharedPrefManager;

public class CourseRegistrationApp extends Application implements Application.ActivityLifecycleCallbacks {

    private static CourseRegistrationApp courseRegistrationApp;
    private NsdHelper nsdHelper;

    @Override
    public void onCreate() {
        super.onCreate();
        courseRegistrationApp = this;
        SharedPrefManager.getSharedPrefInstance().initSharedPref(getApplicationContext());
        registerActivityLifecycleCallbacks(this);
        SharedPrefManager.getSharedPrefInstance().setPort(0);
        SharedPrefManager.getSharedPrefInstance().setIpAddress("");
        nsdHelper = new NsdHelper();
        nsdHelper.initDnsSd(this);
        nsdHelper.browseService();
    }

    public static synchronized CourseRegistrationApp getInstance() {
        return courseRegistrationApp;
    }

    @Override
    public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle savedInstanceState) {

    }

    @Override
    public void onActivityStarted(@NonNull Activity activity) {

    }

    @Override
    public void onActivityResumed(@NonNull Activity activity) {

    }

    @Override
    public void onActivityPaused(@NonNull Activity activity) {

    }

    @Override
    public void onActivityStopped(@NonNull Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(@NonNull Activity activity) {

    }
}
