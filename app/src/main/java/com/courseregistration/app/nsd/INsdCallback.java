package com.courseregistration.app.nsd;

import com.github.druk.rxdnssd.BonjourService;

public interface INsdCallback {
    void onReceiveNsdCallback(BonjourService bonjourService);
}
