package com.courseregistration.app.nsd;

import android.content.Context;
import android.util.Log;

import com.courseregistration.app.Constants;
import com.courseregistration.app.sharedpreference.SharedPrefManager;
import com.github.druk.rxdnssd.BonjourService;
import com.github.druk.rxdnssd.RxDnssd;
import com.github.druk.rxdnssd.RxDnssdBindable;

import java.net.Inet4Address;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;


public class NsdHelper {

    private static final String TAG = "NsdHelper";
    RxDnssd rxdnssd;
    private static INsdCallback nsdCallback;

    public NsdHelper(){
    }

    public void initDnsSd(Context context){
        rxdnssd = new RxDnssdBindable(context);
    }

    public void setListener(INsdCallback nsdCallback){
        NsdHelper.nsdCallback = nsdCallback;
    }

    public void browseService(){
        Subscription subscription = rxdnssd.browse("_http._udp", "local.")
                .compose(rxdnssd.resolve())
                .compose(rxdnssd.queryRecords())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<BonjourService>() {
                    @Override
                    public void call(BonjourService bonjourService) {
                        if(nsdCallback!=null)
                            nsdCallback.onReceiveNsdCallback(bonjourService);

                        Inet4Address address = bonjourService.getInet4Address();
                        String hostIP = "";
                        if(address!=null){
                             hostIP = address.getHostAddress() ;
                        }
                        if(bonjourService.getServiceName().equals(Constants.SERVICE_NAME)){
                            Log.d(TAG, bonjourService.toString() + " " + hostIP + " " + bonjourService.getServiceName());
                            SharedPrefManager.getSharedPrefInstance().setIpAddress(hostIP);
                            SharedPrefManager.getSharedPrefInstance().setPort(bonjourService.getPort());
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        Log.e(TAG, "error", throwable);
                    }
                });
    }
}

