package com.courseregistration.app.sharedpreference;


public class SharedPrefKeys {

    static final String USER_ID = "user_id";
    static final String USER_TYPE = "user_type";
    static final String IP = "ip";
    static final String PORT = "port";


}

