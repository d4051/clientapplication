package com.courseregistration.app.sharedpreference;
import android.content.Context;
import android.content.SharedPreferences;

import com.courseregistration.app.R;


public class SharedPrefManager {

    private static SharedPrefManager instance;
    private SharedPreferences sharedPreferences;

    // Get instance of SharedPrefManager
    public static SharedPrefManager getSharedPrefInstance() {
        if (instance == null)
            instance = new SharedPrefManager();
        return instance;
    }

    private SharedPrefManager() {
        super();
    }

    public void initSharedPref(Context context) {
        sharedPreferences = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
    }

    // Set a string value to SharedPreferences
    private boolean setStringInPreferences(String key, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        return editor.commit();
    }

    // Set an int value to SharedPreferences
    private boolean setIntInPreferences(String key, int value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        return editor.commit();
    }

    // Get a string value from SharedPreferences
    private String getStringFromPreferences(String key) {
        return sharedPreferences.getString(key, "");
    }

    // Get an int value from SharedPreferences
    private int getIntFromPreferences(String key) {
        return sharedPreferences.getInt(key, -1);
    }

    public void setUserId(String userId) {
        setStringInPreferences(SharedPrefKeys.USER_ID, userId);
    }

    public String getUserId() {
        return getStringFromPreferences(SharedPrefKeys.USER_ID);
    }

    public void setUserType(String userType) {
        setStringInPreferences(SharedPrefKeys.USER_TYPE, userType);
    }

    public String getUserType() {
        return getStringFromPreferences(SharedPrefKeys.USER_TYPE);
    }

    public void setIpAddress(String ipAddress) {
        setStringInPreferences(SharedPrefKeys.IP, ipAddress);
    }

    public String getIpAddress() {
        return getStringFromPreferences(SharedPrefKeys.IP);
    }

    public void setPort(int port) {
        setIntInPreferences(SharedPrefKeys.PORT, port);
    }

    public int getPort() {
        return getIntFromPreferences(SharedPrefKeys.PORT);
    }
}

