package com.courseregistration.app.socket;

public interface IUdpDataListener {
    void onReceiveData(String data,String request);
    void onError(String error);
}
