package com.courseregistration.app.socket;

import android.util.Log;

import com.courseregistration.app.sharedpreference.SharedPrefManager;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

public class UdpClient implements Runnable {

    private static final String TAG = UdpClient.class.getName();

    private String host;
    private int port;
    private IUdpDataListener iUdpDataListener;
    private String data;
    private String request;

    public UdpClient(String data,IUdpDataListener iUdpDataListener,String request){
        this.host = SharedPrefManager.getSharedPrefInstance().getIpAddress();
        this.port = SharedPrefManager.getSharedPrefInstance().getPort();
        this.iUdpDataListener = iUdpDataListener;
        this.data = data;
        this.request = request;
    }

    @Override
    public void run() {
        boolean run = true;
        try {
            DatagramSocket udpSocket = new DatagramSocket(port);
            InetAddress serverAddr = InetAddress.getByName(host);
            byte[] buf = data.getBytes();
            DatagramPacket packet = new DatagramPacket(buf, buf.length,serverAddr, port);
            udpSocket.send(packet);
            while (run) {
                try {
                    byte[] message = new byte[8000];
                    packet = new DatagramPacket(message,message.length);
                    Log.i(TAG, "Wait to receive data");
                    udpSocket.setSoTimeout(8000);
                    udpSocket.receive(packet);
                    String text = new String(message, 0, packet.getLength());
                    iUdpDataListener.onReceiveData(text,request);
                    Log.d(TAG, "Received text " + text);
                    run = false;
                    udpSocket.close();
                } catch (IOException e) {
                    Log.e(TAG, "error: ", e);
                    run = false;
                    udpSocket.close();
                    iUdpDataListener.onError(e.toString());
                }
            }
        } catch (SocketException | UnknownHostException e) {
            Log.e(TAG, "Socket Open: Error:", e);
            iUdpDataListener.onError(e.toString());
        } catch (IOException e) {
            e.printStackTrace();
            iUdpDataListener.onError(e.toString());
        }
    }
}
