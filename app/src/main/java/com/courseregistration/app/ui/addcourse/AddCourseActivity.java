package com.courseregistration.app.ui.addcourse;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.courseregistration.app.Constants;
import com.courseregistration.app.R;
import com.courseregistration.app.sharedpreference.SharedPrefManager;
import com.courseregistration.app.socket.IUdpDataListener;
import com.courseregistration.app.socket.UdpClient;

import org.json.JSONException;
import org.json.JSONObject;

public class AddCourseActivity extends AppCompatActivity implements IUdpDataListener {

    ProgressBar progressBar;
    EditText courseName,courseId;
    Spinner creditsSpinner;
    Spinner courseTypeSpinner;
    Button submit;
    final String[] selectCourseTypeItem = new String[1];

    final String[] selectCreditsItem = new String[1];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_course);
        progressBar=(ProgressBar) findViewById(R.id.progressBar);

        courseName = findViewById(R.id.course_name);
        courseId = findViewById(R.id.course_id);
        submit = findViewById(R.id.submit_button);
        creditsSpinner= (Spinner) findViewById(R.id.course_credits_spinner);
        courseTypeSpinner= (Spinner) findViewById(R.id.course_type_spinner);


        creditsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {
                try {

                    selectCreditsItem[0] = parentView.getItemAtPosition(position).toString();

                }
                catch (Exception e) {

                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });



        courseTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {
                try {

                    selectCourseTypeItem[0] = parentView.getItemAtPosition(position).toString();

                }
                catch (Exception e) {

                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(courseName.getText().length() > 0 && selectCourseTypeItem[0].length() >0 && selectCreditsItem[0].length()>0 &&
                        courseId.getText().length() >0){
                    AddNewCourse();
                }
                else {
                    Toast.makeText(AddCourseActivity.this,"Fill all the details.",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    public void AddNewCourse(){
        progressBar.setVisibility(View.VISIBLE);
        JSONObject professor = new JSONObject();
        try {
            professor.put("id", SharedPrefManager.getSharedPrefInstance().getUserId());
            professor.put("user_type", Constants.USER_PROFESSOR);
            professor.put("request", Constants.ADD_COURSE);
            professor.put("request_type", Constants.REQUEST_TYPE_WRITE);
            professor.put("course_name",courseName.getText().toString() );
            professor.put("course_id",courseId.getText().toString() );
            professor.put("course_type",selectCourseTypeItem[0]);
            professor.put("course_credits",selectCreditsItem[0]);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        new Thread(new UdpClient(professor.toString(), AddCourseActivity.this,Constants.ADD_COURSE)).start();

    }

    @Override
    public void onReceiveData(String data, String request) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(AddCourseActivity.this,data,Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onError(String error) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(AddCourseActivity.this,error,Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
    }
}
