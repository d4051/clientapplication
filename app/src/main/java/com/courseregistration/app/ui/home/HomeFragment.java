package com.courseregistration.app.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.courseregistration.app.R;
import com.courseregistration.app.sharedpreference.SharedPrefManager;
import com.courseregistration.app.ui.addcourse.AddCourseActivity;
import com.courseregistration.app.ui.mycourse.MyCoursesActivity;
import com.courseregistration.app.ui.registercourse.RegisterCoursesActivity;
import com.courseregistration.app.ui.viewcoursedetails.ViewCourseDetailsActivity;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        final Button my_courses_button = root.findViewById(R.id.my_courses_button);
        final Button register_button = root.findViewById(R.id.register_button);
        final Button add_course_button = root.findViewById(R.id.add_course_button);
        final Button delete_course_button = root.findViewById(R.id.delete_course_button);
        final Button view_course_button = root.findViewById(R.id.view_course_button);

        if (SharedPrefManager.getSharedPrefInstance().getUserType().equals("Student")){
            add_course_button.setEnabled(false);
            delete_course_button.setEnabled(false);
            view_course_button.setEnabled(false);

            my_courses_button.setEnabled(true);
            register_button.setEnabled(true);
        }
        else if(SharedPrefManager.getSharedPrefInstance().getUserType().equals("Professor")){
            my_courses_button.setEnabled(false);
            register_button.setEnabled(false);

            add_course_button.setEnabled(true);
            delete_course_button.setEnabled(true);
            view_course_button.setEnabled(true);
        }

        else {
            my_courses_button.setEnabled(false);
            register_button.setEnabled(false);

            add_course_button.setEnabled(false);
            delete_course_button.setEnabled(false);
            view_course_button.setEnabled(false);

            Toast.makeText(getActivity(),"Set User Type and User Id in settings to access registration portal",Toast.LENGTH_LONG).show();

        }


        my_courses_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MyCoursesActivity.class);
                startActivity(intent);
            }
        });
        register_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), RegisterCoursesActivity.class);
                startActivity(intent);
            }
        });

        add_course_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AddCourseActivity.class);
                startActivity(intent);
            }
        });


        view_course_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ViewCourseDetailsActivity.class);
                startActivity(intent);
            }
        });

        return root;
    }
}