package com.courseregistration.app.ui.mycourse;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.courseregistration.app.Constants;
import com.courseregistration.app.R;
import com.courseregistration.app.sharedpreference.SharedPrefManager;
import com.courseregistration.app.socket.IUdpDataListener;
import com.courseregistration.app.socket.UdpClient;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.courseregistration.app.Constants.DEREGISTER_COURSE;
import static com.courseregistration.app.Constants.GET_AVAILABLE_COURSES;
import static com.courseregistration.app.Constants.GET_MY_COURSES;
import static com.courseregistration.app.Constants.REGISTER_COURSE;

public class MyCoursesActivity extends AppCompatActivity implements IUdpDataListener {

    ObjectCourseAdapter adapter;
    ProgressBar progressBar;
    ArrayList<ObjectCourse> courses = new ArrayList<>();
    RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_courses);

        progressBar=(ProgressBar) findViewById(R.id.progressBar);

        getMyCourses();
        // data to populate the RecyclerView with

        recyclerView = findViewById(R.id.my_courses_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ObjectCourseAdapter(this, courses, new ObjectCourseAdapter.CustomItemClickListener() {
            @Override
            public void onItemClick(View v, ObjectCourse course, int position) {
                deregisterCourse(course);

                //Toast.makeText(v.getContext(), "ITEM PRESSED = " + course.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        recyclerView.setAdapter(adapter);

    }

    public void getMyCourses(){
        progressBar.setVisibility(View.VISIBLE);
        JSONObject student = new JSONObject();
        try {
            student.put("id", SharedPrefManager.getSharedPrefInstance().getUserId());
            student.put("user_type", Constants.USER_STUDENT);
            student.put("request", GET_MY_COURSES);
            student.put("request_type", Constants.REQUEST_TYPE_READ);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        new Thread(new UdpClient(student.toString(), MyCoursesActivity.this, GET_MY_COURSES)).start();
    }

    public void deregisterCourse(ObjectCourse course){
        progressBar.setVisibility(View.VISIBLE);
        JSONObject student = new JSONObject();
        try {
            student.put("id", SharedPrefManager.getSharedPrefInstance().getUserId());
            student.put("user_type", Constants.USER_STUDENT);
            student.put("request", DEREGISTER_COURSE);
            student.put("request_type", Constants.REQUEST_TYPE_WRITE);
            student.put("course_name", course.getCourse_name());
            student.put("course_id", course.getCourse_id());
            student.put("course_type",course.getCourse_type());
            student.put("course_credits",course.getCourse_credits());

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        new Thread(new UdpClient(student.toString(), MyCoursesActivity.this, DEREGISTER_COURSE)).start();
    }
    @Override
    public void onReceiveData(String data,String request) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(request.equalsIgnoreCase(GET_MY_COURSES)){
                    try {
                        JSONArray jsonArray = new JSONArray(data);
                        for (int i = 0; i < jsonArray.length(); i++)
                        {
                            JSONObject jsonObj = jsonArray.getJSONObject(i);
                            Gson gson=new Gson();
                            ObjectCourse course = gson.fromJson(jsonObj.toString(),ObjectCourse.class);
                            courses.add(course);
                            recyclerView.setAdapter(adapter);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else if (request.equalsIgnoreCase(DEREGISTER_COURSE)){
                    Toast.makeText(MyCoursesActivity.this,data,Toast.LENGTH_SHORT).show();
                    if(data.equalsIgnoreCase("SUCCESS")){
                        courses.clear();
                        recyclerView.setAdapter(adapter);
                        getMyCourses();
                    }
                }
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onError(String error) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MyCoursesActivity.this,error,Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
    }
}
