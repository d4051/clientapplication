package com.courseregistration.app.ui.mycourse;

public class ObjectCourse {
    private String course_name;
    private String course_id;
    private String course_type;
    private String course_credits;

    public ObjectCourse(String course_name, String course_id, String course_type, String course_credits) {
        this.course_name = course_name;
        this.course_id = course_id;
        this.course_type = course_type;
        this.course_credits = course_credits;
    }

    public String getCourse_name() {
        return course_name;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

    public String getCourse_credits() {
        return course_credits;
    }

    public void setCourse_credits(String course_credits) {
        this.course_credits = course_credits;
    }

    public String getCourse_type() {
        return course_type;
    }

    public void setCourse_type(String course_type) {
        this.course_type = course_type;
    }

    public String getCourse_id() {
        return course_id;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    @Override
    public String toString() {
        return "ObjectCourse{" +
                "courseName='" + course_name + '\'' +
                ", credits='" + course_credits + '\'' +
                ", courseType='" + course_type + '\'' +
                ", courseId='" + course_id + '\'' +
                '}';
    }
}
