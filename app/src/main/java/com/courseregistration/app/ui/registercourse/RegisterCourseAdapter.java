package com.courseregistration.app.ui.registercourse;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.courseregistration.app.R;
import com.courseregistration.app.ui.mycourse.ObjectCourse;

import java.util.List;

public class RegisterCourseAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    Context context;
    List<ObjectCourse> objectCourseList;
    CustomItemClickListener listener;

    public RegisterCourseAdapter(Context context, List<ObjectCourse> objectDeviceList, CustomItemClickListener listener) {
        this.objectCourseList = objectDeviceList;
        this.context = context;
        this.listener = listener;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        RecyclerView.ViewHolder viewHolder;

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.register_courses_item, parent, false);
        viewHolder = new CourseLayout(view, parent.getContext());
        return viewHolder;

    }
    public interface CustomItemClickListener {

        void onItemClick(View v,ObjectCourse course, int position);

    }

    public interface CustomItemLongClickListener {

        void onItemLongClick(View v, int position);

    }

    public static class CourseLayout extends RecyclerView.ViewHolder {

        TextView tv_course_name,tv_credits,tv_course_type,tv_course_id;
        Button button_register;
        CourseLayout(View listItemView, Context context) {
            super(listItemView);

            tv_course_name = listItemView.findViewById(R.id.course_name);
            tv_course_id = listItemView.findViewById(R.id.course_id);
            tv_credits = listItemView.findViewById(R.id.course_credits);
            tv_course_type = listItemView.findViewById(R.id.course_type);
            button_register = listItemView.findViewById(R.id.register_btn);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        CourseLayout courseLayout = (CourseLayout) holder;
        final ObjectCourse course = objectCourseList.get(position);

        courseLayout.tv_course_name.setText(course.getCourse_name());
        courseLayout.tv_credits.setText(course.getCourse_credits());
        courseLayout.tv_course_type.setText(course.getCourse_type());
        courseLayout.tv_course_id.setText(course.getCourse_id());

        courseLayout.button_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, course,position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return objectCourseList.size();
    }
}
