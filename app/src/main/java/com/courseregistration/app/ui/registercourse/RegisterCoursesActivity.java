package com.courseregistration.app.ui.registercourse;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.courseregistration.app.Constants;
import com.courseregistration.app.R;
import com.courseregistration.app.sharedpreference.SharedPrefManager;
import com.courseregistration.app.socket.IUdpDataListener;
import com.courseregistration.app.socket.UdpClient;
import com.courseregistration.app.ui.mycourse.ObjectCourse;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.courseregistration.app.Constants.DEREGISTER_COURSE;
import static com.courseregistration.app.Constants.GET_AVAILABLE_COURSES;
import static com.courseregistration.app.Constants.REGISTER_COURSE;

public class RegisterCoursesActivity extends AppCompatActivity implements IUdpDataListener {

    RegisterCourseAdapter adapter;
    ProgressBar progressBar;
    RecyclerView recyclerView;
    ArrayList<ObjectCourse> courses = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_courses);
        progressBar=(ProgressBar) findViewById(R.id.progressBar);
        getAvailableCourses();
        // data to populate the RecyclerView with

        recyclerView = findViewById(R.id.register_courses_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new RegisterCourseAdapter(this, courses, new RegisterCourseAdapter.CustomItemClickListener() {
            @Override
            public void onItemClick(View v, ObjectCourse course, int position) {
                registerCourse(course);
            }
        });
        recyclerView.setAdapter(adapter);
    }

    public void getAvailableCourses(){
        progressBar.setVisibility(View.VISIBLE);
        JSONObject student = new JSONObject();
        try {
            student.put("id", SharedPrefManager.getSharedPrefInstance().getUserId());
            student.put("user_type", Constants.USER_STUDENT);
            student.put("request", GET_AVAILABLE_COURSES);
            student.put("request_type", Constants.REQUEST_TYPE_READ);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        new Thread(new UdpClient(student.toString(), RegisterCoursesActivity.this, GET_AVAILABLE_COURSES)).start();
    }

    public void registerCourse(ObjectCourse course){
        progressBar.setVisibility(View.VISIBLE);

        JSONObject student = new JSONObject();
        try {
            student.put("id", SharedPrefManager.getSharedPrefInstance().getUserId());
            student.put("user_type", Constants.USER_STUDENT);
            student.put("request_type", Constants.REQUEST_TYPE_WRITE);
            student.put("request", Constants.REGISTER_COURSE);
            student.put("course_name", course.getCourse_name());
            student.put("course_id", course.getCourse_id());
            student.put("course_type",course.getCourse_type());
            student.put("course_credits",course.getCourse_credits());

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        new Thread(new UdpClient(student.toString(), RegisterCoursesActivity.this,Constants.REGISTER_COURSE)).start();
    }

    @Override
    public void onReceiveData(String data,String request) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(request.equalsIgnoreCase(GET_AVAILABLE_COURSES)){
                    try {
                        JSONArray jsonArray = new JSONArray(data);
                        for (int i = 0; i < jsonArray.length(); i++)
                        {
                            JSONObject jsonObj = jsonArray.getJSONObject(i);
                            Gson gson=new Gson();
                            ObjectCourse course = gson.fromJson(jsonObj.toString(),ObjectCourse.class);
                            courses.add(course);
                            recyclerView.setAdapter(adapter);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else if (request.equalsIgnoreCase(REGISTER_COURSE)){
                    Toast.makeText(RegisterCoursesActivity.this,data,Toast.LENGTH_SHORT).show();
                    if(data.equalsIgnoreCase("SUCCESS")){
                        courses.clear();
                        recyclerView.setAdapter(adapter);
                        getAvailableCourses();
                    }
                }
                progressBar.setVisibility(View.GONE);

            }
        });

    }

    @Override
    public void onError(String error) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(RegisterCoursesActivity.this,error,Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);

            }
        });
    }
}
