package com.courseregistration.app.ui.settings;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.courseregistration.app.Constants;
import com.courseregistration.app.R;
import com.courseregistration.app.nsd.INsdCallback;
import com.courseregistration.app.nsd.NsdHelper;
import com.courseregistration.app.sharedpreference.SharedPrefManager;
import com.courseregistration.app.ui.mycourse.MyCoursesActivity;
import com.github.druk.rxdnssd.BonjourService;

import java.net.Inet4Address;
import java.util.Arrays;

public class SettingsFragment extends Fragment implements INsdCallback {

    private SettingsViewModel settingsViewModel;
    NsdHelper nsdHelper;
    TextView dns_sd_info;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        settingsViewModel =
                new ViewModelProvider(this).get(SettingsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_settings, container, false);

        Spinner userTypeSpinner;
        userTypeSpinner= (Spinner) root.findViewById(R.id.user_type_spinner);

        EditText userId;
        userId = root.findViewById(R.id.edit_user_id);

        Button saveButton;
        saveButton = root.findViewById(R.id.save_button);

        dns_sd_info = root.findViewById(R.id.dns_sd_info);

        dns_sd_info.setText("Service name: " + Constants.SERVICE_NAME + "\nIP: " + SharedPrefManager.getSharedPrefInstance().getIpAddress() +
                "\nPort: " + SharedPrefManager.getSharedPrefInstance().getPort());

        String[] userTypes = getResources().getStringArray(R.array.spinnerItems);
        userTypeSpinner.setSelection(Arrays.asList(userTypes).indexOf(SharedPrefManager.getSharedPrefInstance().getUserType()));
        userId.setText(SharedPrefManager.getSharedPrefInstance().getUserId());
        final String[] selectItem = new String[1];
        userTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {
                try {

                     selectItem[0] = parentView.getItemAtPosition(position).toString();

                }
                catch (Exception e) {

                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(userId.getText().length() <=0){
                    Toast.makeText(getActivity(),"Enter User ID",Toast.LENGTH_LONG).show();
                }
                else {
                    SharedPrefManager.getSharedPrefInstance().setUserId(userId.getText().toString());
                    SharedPrefManager.getSharedPrefInstance().setUserType(selectItem[0].toString());

                    Toast.makeText(getActivity(),"Details Saved",Toast.LENGTH_LONG).show();

                }
            }
        });

        nsdHelper = new NsdHelper();
        nsdHelper.setListener(this);
        return root;
    }


    @Override
    public void onReceiveNsdCallback(BonjourService bonjourService) {
//        Toast.makeText(getActivity(),"IP: " + bonjourService.getInet4Address() + "\nService name: " + bonjourService.getServiceName()
//                + "\nPort: " + bonjourService.getPort(),Toast.LENGTH_LONG).show();
        Inet4Address address = bonjourService.getInet4Address();
        String hostIP = "";
        if(address!=null){
            hostIP = address.getHostAddress() ;
        }
        dns_sd_info.setText("Service name: " + bonjourService.getServiceName() + "\nIP: " + hostIP + "\nPort: " + bonjourService.getPort());
    }
}