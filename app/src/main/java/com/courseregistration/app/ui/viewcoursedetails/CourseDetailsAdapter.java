package com.courseregistration.app.ui.viewcoursedetails;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.courseregistration.app.R;
import com.courseregistration.app.ui.mycourse.ObjectCourse;

import java.util.List;


public class CourseDetailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    Context context;
    List<ObjectCourseDetails> objectCourseDetailsList;
    CustomItemClickListener listener;

    public CourseDetailsAdapter(Context context, List<ObjectCourseDetails> objectCourseDetailsList) {
        this.objectCourseDetailsList = objectCourseDetailsList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        RecyclerView.ViewHolder viewHolder;

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_course_details_item, parent, false);
        viewHolder = new CourseLayout(view, parent.getContext());

        final RecyclerView.ViewHolder finalViewHolder = viewHolder;
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //listener.onItemClick(v, finalViewHolder.getLayoutPosition());
            }
        });

        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                //longClickListener.onItemLongClick(view, finalViewHolder.getLayoutPosition());
                return false;
            }
        });



        return viewHolder;

    }
    public interface CustomItemClickListener {

        void onItemClick(View v,ObjectCourse course, int position);

    }

    public interface CustomItemLongClickListener {

        void onItemLongClick(View v, int position);

    }

    public static class CourseLayout extends RecyclerView.ViewHolder {

        TextView tv_course_name,tv_credits,tv_course_type,tv_total_students,tv_course_id;
        CourseLayout(View listItemView, Context context) {
            super(listItemView);

            tv_course_name = listItemView.findViewById(R.id.course_name);
            tv_course_id = listItemView.findViewById(R.id.course_id);
            tv_credits = listItemView.findViewById(R.id.course_credits);
            tv_course_type = listItemView.findViewById(R.id.course_type);
            tv_total_students = listItemView.findViewById(R.id.no_of_students);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        CourseLayout courseLayout = (CourseLayout) holder;
        final ObjectCourseDetails course = objectCourseDetailsList.get(position);

        courseLayout.tv_course_name.setText(course.getCourseName());
        courseLayout.tv_course_id.setText(course.getCourse_id());
        courseLayout.tv_credits.setText(course.getCourse_credits());
        courseLayout.tv_course_type.setText(course.getCourse_type());
        courseLayout.tv_total_students.setText(String.valueOf(course.getNo_of_students()));
    }

    @Override
    public int getItemCount() {
        return objectCourseDetailsList.size();
    }
}
