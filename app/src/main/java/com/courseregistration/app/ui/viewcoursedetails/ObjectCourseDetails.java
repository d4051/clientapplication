package com.courseregistration.app.ui.viewcoursedetails;


public class ObjectCourseDetails {
    private String course_name;
    private String course_id;
    private String course_type;
    private String course_credits;
    private int no_of_students;

    public ObjectCourseDetails(String course_name, String course_id, String course_type, String course_credits, int no_of_students) {
        this.course_name = course_name;
        this.course_id = course_id;
        this.course_type = course_type;
        this.course_credits = course_credits;
        this.no_of_students = no_of_students;
    }

    public int getNo_of_students() {
        return no_of_students;
    }

    public void setNo_of_students(int no_of_students) {
        this.no_of_students = no_of_students;
    }

    public String getCourseName() {
        return course_name;
    }

    public void setCourseName(String courseName) {
        this.course_name = courseName;
    }

    public String getCourse_credits() {
        return course_credits;
    }

    public void setCourse_credits(String course_credits) {
        this.course_credits = course_credits;
    }

    public String getCourse_type() {
        return course_type;
    }

    public void setCourse_type(String course_type) {
        this.course_type = course_type;
    }

    public String getCourse_id() {
        return course_id;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    @Override
    public String toString() {
        return "ObjectCourseDetails{" +
                "courseName='" + course_name + '\'' +
                ", credits='" + course_credits + '\'' +
                ", courseType='" + course_type + '\'' +
                ", totalStudents=" + no_of_students +
                ", courseId='" + course_id + '\'' +
                '}';
    }
}
