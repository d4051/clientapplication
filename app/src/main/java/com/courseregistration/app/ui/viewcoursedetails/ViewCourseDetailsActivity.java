package com.courseregistration.app.ui.viewcoursedetails;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.courseregistration.app.Constants;
import com.courseregistration.app.R;
import com.courseregistration.app.sharedpreference.SharedPrefManager;
import com.courseregistration.app.socket.IUdpDataListener;
import com.courseregistration.app.socket.UdpClient;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ViewCourseDetailsActivity extends AppCompatActivity implements IUdpDataListener {

    CourseDetailsAdapter adapter;
    ProgressBar progressBar;
    ArrayList<ObjectCourseDetails> courses = new ArrayList<>();
    RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_course_details);
        progressBar=(ProgressBar) findViewById(R.id.progressBar);

        // data to populate the RecyclerView with

        recyclerView = findViewById(R.id.view_course_details_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CourseDetailsAdapter(this, courses);
        recyclerView.setAdapter(adapter);


        ViewCourseDetails();
    }

    public void ViewCourseDetails(){
        progressBar.setVisibility(View.VISIBLE);
        JSONObject professor = new JSONObject();
        try {
            professor.put("id", SharedPrefManager.getSharedPrefInstance().getUserId());
            professor.put("user_type", Constants.USER_PROFESSOR);
            professor.put("request", Constants.VIEW_COURSE_DETAILS);
            professor.put("request_type", Constants.REQUEST_TYPE_READ);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        new Thread(new UdpClient(professor.toString(), ViewCourseDetailsActivity.this,Constants.VIEW_COURSE_DETAILS)).start();

    }

    @Override
    public void onReceiveData(String data, String request) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    JSONArray jsonArray = new JSONArray(data);
                    for (int i = 0; i < jsonArray.length(); i++)
                    {
                        JSONObject jsonObj = jsonArray.getJSONObject(i);
                        Gson gson=new Gson();
                        ObjectCourseDetails courseDetails = gson.fromJson(jsonObj.toString(),ObjectCourseDetails.class);
                        courses.add(courseDetails);
                        recyclerView.setAdapter(adapter);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onError(String error) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(ViewCourseDetailsActivity.this,error,Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
    }
}
